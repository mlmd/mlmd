---
title: Ab initio study of shock-compressed copper
authors:
- Maximilian Schörner
- Bastian B. L. Witte
- Andrew D. Baczewski
- Attila Cangi
- Ronald Redmer
date: '2022-08-01'
publishDate: '2024-04-18T06:56:53.547266Z'
publication_types:
- article-journal
publication: '*Phys. Rev. B*'
doi: 10.1103/PhysRevB.106.054304
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevB.106.054304
---
