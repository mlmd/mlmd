---
title: The relevance of electronic perturbations in the warm dense electron gas
authors:
- Zhandos Moldabekov
- Tobias Dornheim
- Maximilian Böhme
- Jan Vorberger
- Attila Cangi
date: '2021-09-01'
publishDate: '2024-04-18T06:56:53.477794Z'
publication_types:
- article-journal
publication: '*The Journal of Chemical Physics*'
doi: 10.1063/5.0062325
links:
- name: URL
  url: http://dx.doi.org/10.1063/5.0062325
---
