---
title: Accelerating finite-temperature Kohn-Sham density functional theory with deep
  neural networks
authors:
- J. A. Ellis
- L. Fiedler
- G. A. Popoola
- N. A. Modine
- J. A. Stephens
- A. P. Thompson
- A. Cangi
- S. Rajamanickam
date: '2021-07-01'
publishDate: '2024-04-18T06:56:53.471446Z'
publication_types:
- article-journal
publication: '*Phys. Rev. B*'
doi: 10.1103/PhysRevB.104.035120
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevB.104.035120
---
