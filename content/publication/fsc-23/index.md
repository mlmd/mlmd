---
title: Machine-Learning for Static and Dynamic Electronic Structure Theory
authors:
- Lenz Fiedler
- Karan Shah
- Attila Cangi
date: '2023-01-01'
publishDate: '2024-04-18T06:56:53.346939Z'
publication_types:
- chapter
publication: '*Machine Learning in Molecular Sciences*'
doi: 10.1007/978-3-031-37196-7_5
links:
- name: URL
  url: http://dx.doi.org/10.1007/978-3-031-37196-7_5
---
