---
title: atoMEC
authors:
- Timothy Callow
- Daniel Kotik
- Ekaterina Tsvetoslavova Stankulova
- Nathan Rahat
- Eli Kraisler
- Attila Cangi
date: '2023-01-01'
publishDate: '2024-04-18T06:56:53.340320Z'
publication_types:
- manuscript
publication: '*Zenodo*'
doi: 10.5281/ZENODO.10090435
tags:
- density-functional-theory
- plasma-physics
- electronic-structure
- atomic-physics
- warm-dense-matter
links:
- name: URL
  url: https://zenodo.org/doi/10.5281/zenodo.10090435
---
