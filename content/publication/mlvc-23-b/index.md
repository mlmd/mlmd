---
title: Assessing the accuracy of hybrid exchange-correlation functionals for the density
  response of warm dense electrons
authors:
- Zhandos A. Moldabekov
- Mani Lokamani
- Jan Vorberger
- Attila Cangi
- Tobias Dornheim
date: '2023-03-01'
publishDate: '2024-04-18T06:56:53.586686Z'
publication_types:
- article-journal
publication: '*The Journal of Chemical Physics*'
doi: 10.1063/5.0135729
links:
- name: URL
  url: http://dx.doi.org/10.1063/5.0135729
---
