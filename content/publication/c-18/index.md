---
title: An Exchange-Correlation Functional Capturing Bulk Surface and Confinement Physics
authors:
- Attila Cangi
date: '2018-07-01'
publishDate: '2024-04-18T06:56:53.359247Z'
publication_types:
- article-journal
doi: 10.2172/1530147
links:
- name: URL
  url: https://www.osti.gov/biblio/1530147
---
