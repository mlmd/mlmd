---
title: First-principles derivation and properties of density-functional average-atom
  models
authors:
- T. J. Callow
- S. B. Hansen
- E. Kraisler
- A. Cangi
date: '2022-04-01'
publishDate: '2024-04-18T06:56:53.528681Z'
publication_types:
- article-journal
publication: '*Phys. Rev. Res.*'
doi: 10.1103/PhysRevResearch.4.023055
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevResearch.4.023055
---
